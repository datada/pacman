# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s,s,w,s,w,w,s,w]

# ( (state -> state) , state ) -> state
def treeSearch(nextPlaceStrategy, startState):
    state = startState
    foundState = None
    whereNext = None
    moreToLook = False
    foundIt = False

    while True:
        whereNext = moreToLook = foundIt = nextPlaceStrategy( state )

        if (True == foundIt):
            foundState = state
            break

        if (False == moreToLook):
            break

        state = whereNext
        # print state

    return foundState

# the main strategy is encoded in toBeVisited .pop and .push
# (ADT, Set, problem) -> (state -> state)
def makeStrategy(toBeVisited, alreadyVisited, problem):
    # state -> state
    def strategy(here): 
        position = here[0]
        move = here[1]
        step = here[2]
    
        for state in problem.getSuccessors( position ):
            if state in alreadyVisited:
                print 'already visited'
            else:
                print 'adding legal move'
                alreadyVisited.add( state )
                toBeVisited.push( (state[0], move+' '+state[1], step+state[2]) )
        if problem.isGoalState( position ):
            print '+++++++++ Bingo +++++++++++'
            return True
        
        if toBeVisited.isEmpty():
            print '+++++++++ Out of places to look +++++++++++'
            return False
        
        return toBeVisited.pop()
    
    return strategy


# problem -> solution
def depthFirstSearch(problem):

    # print "Start:", problem.getStartState()
    # print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    # print "Start's successors:", problem.getSuccessors(problem.getStartState())
    
    # LIFO via Stack
    dfStrategy = makeStrategy( util.Stack(), set([]), problem )
    
    # problem.getStartState() is converted to 'fancier state' ((x, y), '', 0)
    # we need to take out moves out of the 'found' state
    # ( (x,y), ' West East South ', 45)[1].strip().split(' ') -> ['West', 'East', 'South']
    solution = treeSearch(dfStrategy, (problem.getStartState(), '', 0))[1].strip().split(' ')
    return solution

# problem -> solution
def breadthFirstSearch(problem):
    # FIFO via Queue    
    bfStrategy = makeStrategy( util.Queue(), set([]), problem )
    
    solution = treeSearch(bfStrategy, (problem.getStartState(), '', 0))[1].strip().split(' ')
    return solution

def uniformCostSearch(problem):
    # via PriorityQueue    
    def pf(item):
        # .push(state[0], move+' '+state[1], step+state[2]),
                    # step+state[2])
        return item[2]

    ucStrategy = makeStrategy( util.PriorityQueueWithFunction(pf), set([]), problem )
    
    solution = treeSearch(ucStrategy, (problem.getStartState(), '', 0))[1].strip().split(' ')
    return solution

# (state, problem) -> Positive Number
def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    # via PriorityQueue    
    def pf(item):
        # .push(state[0], move+' '+state[1], step+state[2]),
                    # step+state[2]+ heuristic(state[0], problem))
        return item[2] + heuristic(item[0], problem)

    aStrategy = makeStrategy( util.PriorityQueueWithFunction(pf), set([]), problem )
    
    solution = treeSearch(aStrategy, (problem.getStartState(), '', 0))[1].strip().split(' ')
    return solution


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
