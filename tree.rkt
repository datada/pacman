#lang racket

;;tree as nested list

(define my-tree (list 4 (list 5 7) 2))

;; what about '()?
(define (leaf? tree)
  (not (pair? tree)))

(define (count-leaves tree)
  (cond ((null? tree) 0)
        ((leaf? tree) 1)
        (else (+ (count-leaves (car tree))
                 (count-leaves (cdr tree))))))

(length my-tree)

(count-leaves my-tree)

(define (tree-map proc tree)
  (if (null? tree)
      '()
      (if (leaf? tree)
          (proc tree)
          (cons (tree-map proc (car tree))
                (tree-map proc (cdr tree))))))

(tree-map (lambda (x) (* x x))
          my-tree)

(define (tree-manip leaf-op init merge tree)
  (if (null? tree)
      init
      (if (leaf? tree)
          (leaf-op tree)
          (merge (tree-manip leaf-op init merge (car tree))
                 (tree-manip leaf-op init merge (cdr tree))))))

;;tree-map
(tree-manip (lambda (x) (* x x))
            '()
            cons
            my-tree)

;;count-leaves
(tree-manip (lambda (x) 1)
            0
            +
            my-tree)

;;deep reverse
(tree-manip (lambda (x) x)
            '()
            (lambda (a b)
              (append b (list a)))
            '(1 (2 (3 4) 5) 6))

;;enumerate
(tree-manip (lambda (x) (list x))
            '()
            (lambda (a b)
              (append a b))
            '(1 (2 (3 4) 5) 6))

;;label (depth first)
(tree-manip ((lambda (init)
               (lambda (x)
                 (set! init (add1 init))
                 init)) 0)
            '()
            cons
            '(a (b (c d) e) f))

;; BF Traversal
;; (a (b (c d) e) f)) -> (a f b e c d)
(define (bftrav tree)
  (if (null? tree)
      '()
      (if (leaf? tree)
          tree
          (if (leaf? (car tree))
              (cons (car tree) (bftrav (cdr tree)))
              (map bftrav
                   (append (cdr tree)
                           (bftrav (car tree))))))))
                    
          
(bftrav '())
(bftrav '(a))
(bftrav '(a b))
(bftrav '(a (b)))
(bftrav '((a) b))
(bftrav '(a (b c) d))
(bftrav '(a (b (c d) e) f))

;; BF Label
;; (a (b (c d) e) f) -> (1 (3 (5 6) 4) 2)
(define (bfl* i ts)
  (if (null? ts)
      (list i '())
      (if (leaf? (car ts))
          (match (bfl* (add1 i) (cdr ts))
            [(list j x) (list j (cons i x))])
          (match (bfl* i (cdr ts)) ;;label the rest
            [(list j x) (match (bfl* j (car ts))
                          [(list k y) (list k (cons y x))])]))))

(define (bfl ts)
  (match (bfl* 1 ts)
    [(list k x) x]))

(bfl* 1 '())
(bfl* 1 '(a))
(bfl* 1 '(a b))
(bfl* 1 '((a)))
(bfl* 1 '(a (b)))
(bfl '(a (b (c d) e) f))

