module Pacman where

makeMaze :: Int -> Int -> [[Int]]
makeMaze r c = fmap (\n -> makeNofX n 1) (makeNofX r c)

makeNofX :: Int -> Int -> [Int]
makeNofX n x = fmap (\_ -> x)  (take n [1..])
