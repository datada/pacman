
# Cmp a => (Matrix, a) -> Matrix
def sense(p, Z):
    probs = 0.
    q = []
    for r in range(len(p)):
        q.append([])
        for c in range(len(p[0])):
            hit = (Z == colors[r][c])
            prob = p[r][c] * (hit * pHit + (1-hit) * pMiss)
            # print "sense tally prob %s" % prob
            probs += prob
            q[r].append(prob)
    # print probs
    # normalize
    for r in range(len(p)):
        for c in range(len(p[0])):
            q[r][c] /= probs
            
    return q

# [0,0] no move
#[0,1] right
#[0,-1] left
#[1,0] down
#[-1,0] up
def move(p, U):
    q = []
    for r in range(len(p)):
        # print "adding row"
        q.append([])
        for c in range(len(p[0])):
            # print "calc col elem"
            prob = p_move * p[(r-U[0])%len(p)][(c-U[1])%len(p[0])]
            prob += (1-p_move)* p[r][c]
            # print "adding col elem"
            q[r].append(prob)
    return q

if __name__ == '__main__':

    isDev = True

    def show(p):
        for i in range(len(p)):
            print p[i]

    def closeEnough(a, b):
        return abs(a - b) < 0.0001

    # colors = [['red', 'green', 'green', 'red' , 'red'],
    #           ['red', 'red', 'green', 'red', 'red'],
    #           ['red', 'red', 'green', 'green', 'red'],
    #           ['red', 'red', 'red', 'red', 'red']]

    # measurements = ['green', 'green', 'green' ,'green', 'green']
    # motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]

    colors = [['green', 'green', 'green'],
              ['green', 'red', 'red'],
              ['green', 'green', 'green']]

    measurements = ['red', 'red']
    motions = [[0,0], [0,1]]

    sensor_right = 1.0

    p_move = 0.5

    def show(p):
        for i in range(len(p)):
            print p[i]

    #DO NOT USE IMPORT
    #ENTER CODE BELOW HERE
    #ANY CODE ABOVE WILL CAUSE
    #HOMEWORK TO BE GRADED
    #INCORRECT

    isDev = False
    def log(v):
        if isDev:
            print v
        else:
            pass

    # initial uniform distribution
    count = len(colors) * len(colors[0])
    p = []
    for r in range(len(colors)):
    	p.append([])
    	for c in range(len(colors[0])):
    		p[r].append(1./count)

    # show(p)

    pHit = sensor_right
    pMiss =  1 - pHit



    for i in range(len(motions)):
        p = move(p, motions[i])
        # show(p)
        p = sense(p, measurements[i])
        # show(p)
        

    show(p)


