# code as submitted (hastly without much testing due to lack of time)
# excuse is that never got an email from Udacity so started a week late....and barely made the deadline

colors = [['red', 'green', 'green', 'red' , 'red'],
          ['red', 'red', 'green', 'red', 'red'],
          ['red', 'red', 'green', 'green', 'red'],
          ['red', 'red', 'red', 'red', 'red']]

measurements = ['green', 'green', 'green' ,'green', 'green']


motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]

sensor_right = 0.7

p_move = 0.8

def show(p):
    for i in range(len(p)):
        print p[i]

#DO NOT USE IMPORT
#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

isDev = False
def log(v):
    if isDev:
        print v
    else:
        pass

# Matrix is a list of list .. at the least [[0]]
def matrixSize(m):
    return (len(m), len(m[0]))

# (Num, Num, (Int, Int)-> Num) -> Matrix
def buildMatrix(R, C, proc):
    m = []
    for r in range(R):
        newRow = []
        for c in range(C):
            newRow.append( proc(r,c) )
        m.append( newRow )

    return m

# (Matrix, Num -> Num) -> Matrix
def matrixMap(old, proc):
    (R,C) = matrixSize(old)
    bproc = lambda r,c: proc( old[r][c] )
    return buildMatrix(R,C, bproc)

# Matrix -> Matrix
def cloneMatrix(old):
    proc = lambda v: v
    return matrixMap(old, proc)

# (Matrix, (Int, Int, Num) -> Num) -> None
def matrixEach(old, proc):
    (R,C) = matrixSize(old)
    for r in range(R):
        for c in range(C):
            proc( r, c, old[r][c] )
    return None

def matrixFold(old, acc, proc):
    (R,C) = matrixSize(old)
    for r in range(R):
        for c in range(C):
            acc = proc( acc, old[r][c] )
    return acc

# ([a], Int)-> [a]
def cycle(olds, steps):
    news = olds[:]
    for i in range(len(olds)):
        news[i] = olds[ (i - steps) % len(olds) ]
    return news

# (Matrix, Int)-> Matrix
def matrixCycle(olds, steps):
    ud = steps[0]
    lr = steps[1]
    if 0 != ud:
        log( 'up down %s' % ud )
        return cycle(olds, ud) #swap rows
    elif 0 != lr:
        log( 'left right %s' % lr )
        news = []
        for row in olds:
            news.append( cycle( row, lr) )
        return news
    else:
        log( 'not moving' )
        return cloneMatrix(olds)


(R,C) = matrixSize( colors )

# initialize to uniform dist
initProb = 1.0 /(R*C)
initProc = lambda r,c: initProb
p = buildMatrix(R,C,initProc)


def sumUp(mat):
    adder = lambda acc, v: acc + v
    return matrixFold(mat, 0, adder)

pHit = sensor_right
pMiss =  1 - pHit

pExact = p_move
pOvershoot = (1-pExact)/2
pUndershoot = (1-pExact)/2

# Cmp a => (Matrix, a) -> Matrix
def sense(p, Z):
    (R,C) = matrixSize(p)
    def proc(r,c):
        hit = (Z == colors[r][c])
        return p[r][c] * (hit * pHit + (1-hit) * pMiss)

    q = buildMatrix(R, C, proc)
    s = sumUp(q)
    def normalizer(v):
        return v/s

    return matrixMap(q, normalizer)

def underMove(U):
    ud = U[0]
    lr = U[1]
    if ud < 0:
        ud +=1
    elif 0 < ud:
        ud -=1
    if lr < 0:
        lr +=1
    elif 0 < lr:
        lr -= 1
    return (ud, lr)

def overMove(U):
    ud = U[0]
    lr = U[1]
    if ud < 0:
        ud -=1
    elif 0 < ud:
        ud +=1
    if lr < 0:
        lr -=1
    elif 0 < lr:
        lr += 1
    return (ud, lr) 
    
def move(p, U):
    moved_p = matrixCycle(p, U)
    under_p = matrixCycle(p, underMove(U))
    over_p = matrixCycle(p, overMove(U))
    def proc(r,c):
        s = pExact * moved_p[r][c]
        s += pOvershoot * over_p[r][c]
        s += pUndershoot * under_p[r][c]

        return s
        
    return buildMatrix(R,C, proc)

for i in range(len(motions)):
    p = move(p, motions[i])
    p = sense(p, measurements[i])
    

show(p)


