from mat import *
from cycle import *

# Cmp a => (Matrix, a) -> Matrix
def sense(p, Z):
    (R,C) = matrixSize(p)
    def proc(r,c):
        hit = (Z == colors[r][c])
        return p[r][c] * (hit * pHit + (1-hit) * pMiss)

    q = buildMatrix(R, C, proc)
    s = matrixSumUp(q)
    def normalizer(v):
        return v/s

    return matrixMap(q, normalizer)

def underMove(U):
    ud = U[0]
    lr = U[1]
    if ud < 0:
        ud +=1
    elif 0 < ud:
        ud -=1
    if lr < 0:
        lr +=1
    elif 0 < lr:
        lr -= 1
    return (ud, lr)

def overMove(U):
    ud = U[0]
    lr = U[1]
    if ud < 0:
        ud -=1
    elif 0 < ud:
        ud +=1
    if lr < 0:
        lr -=1
    elif 0 < lr:
        lr += 1
    return (ud, lr) 
    
def move(p, U):
    moved_p = matrixCycle(p, U)
    under_p = matrixCycle(p, underMove(U))
    over_p = matrixCycle(p, overMove(U))
    def proc(r,c):
        s = pExact * moved_p[r][c]
        s += pOvershoot * over_p[r][c]
        s += pUndershoot * under_p[r][c]

        return s
        
    return buildMatrix(R,C, proc)

if __name__ == '__main__':

    isDev = True

    def show(p):
        for i in range(len(p)):
            print p[i]

    def closeEnough(a, b):
        return abs(a - b) < 0.0001

    # colors = [['red', 'green', 'green', 'red' , 'red'],
    #           ['red', 'red', 'green', 'red', 'red'],
    #           ['red', 'red', 'green', 'green', 'red'],
    #           ['red', 'red', 'red', 'red', 'red']]

    # measurements = ['green', 'green', 'green' ,'green', 'green']
    # motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]

    colors = [['green', 'green', 'green'],
              ['green', 'red', 'red'],
              ['green', 'green', 'green']]

    measurements = ['red', 'red']
    motions = [[0,0], [0,1]]

    sensor_right = 1.0

    p_move = 1.0

    def show(p):
        for i in range(len(p)):
            print p[i]

    #DO NOT USE IMPORT
    #ENTER CODE BELOW HERE
    #ANY CODE ABOVE WILL CAUSE
    #HOMEWORK TO BE GRADED
    #INCORRECT

    isDev = False
    def log(v):
        if isDev:
            print v
        else:
            pass


    (R,C) = matrixSize( colors )

    # initialize to uniform dist
    initProb = 1.0 /(R*C)
    initProc = lambda r,c: initProb
    p = buildMatrix(R,C,initProc)


    pHit = sensor_right
    pMiss =  1 - pHit

    pExact = p_move
    pOvershoot = (1-pExact)/2
    pUndershoot = (1-pExact)/2



    for i in range(len(motions)):
        p = move(p, motions[i])
        p = sense(p, measurements[i])
        

    show(p)


