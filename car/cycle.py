from mat import *

def cycle(olds, steps):
	news = olds[:]
	for i in range(len(olds)):
		news[i] = olds[ (i - steps) % len(olds) ]
	return news

def matrixCycle(olds, steps):
	ud = steps[0]
	lr = steps[1]
	if 0 == lr:
		# print 'up down %s' % ud
		return cycle(olds, ud) #swap rows
	else:
		news = []
		for row in olds:
			news.append( cycle( row, lr) )
		return news

if __name__ == '__main__':

    isDev = True

    def show(p):
        for i in range(len(p)):
            print p[i]

    def closeEnough(a, b):
        return abs(a - b) < 0.0001

    a = buildMatrix(5,5, (lambda r,c: r*c))

    show(a)
    print 'one step down'
    show( matrixCycle(a, (1,0)) ) 

    print 'one steps up'
    show( matrixCycle(a, (-1,0)) )

    print 'reset'
    show(a)
    print 'one step right'
    show( matrixCycle(a, (0,1)) ) 

    print 'one steps left'
    show( matrixCycle(a, (0,-1)) )



