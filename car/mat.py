
# Matrix is a list of list .. at the least [[0]]
def matrixSize(m):
    return (len(m), len(m[0]))

# (Num, Num, (Int, Int)-> Num) -> Matrix
def buildMatrix(R, C, proc):
    m = []
    for r in range(R):
        newRow = []
        for c in range(C):
            newRow.append( proc(r,c) )
        m.append( newRow )

    return m

# (Matrix, Num -> Num) -> Matrix
def matrixMap(old, proc):
    (R,C) = matrixSize(old)
    bproc = lambda r,c: proc( old[r][c] )
    return buildMatrix(R,C, bproc)

# Matrix -> Matrix
def cloneMatrix(old):
    proc = lambda v: v
    return matrixMap(old, proc)

# (Matrix, (Int, Int, Num) -> Num) -> None
def matrixEach(old, proc):
    (R,C) = matrixSize(old)
    for r in range(R):
        for c in range(C):
            proc( r, c, old[r][c] )
    return None

def matrixFold(old, acc, proc):
    (R,C) = matrixSize(old)
    for r in range(R):
        for c in range(C):
            acc = proc( acc, old[r][c] )
    return acc

def matrixSumUp(m):
    adder = lambda acc, v: acc + v
    return matrixFold(m, 0, adder)


isDev = False
def log(v):
    if isDev:
        print v
    else:
        pass

if __name__ == '__main__':

    isDev = True

    def show(p):
        for i in range(len(p)):
            print p[i]

    def closeEnough(a, b):
        return abs(a - b) < 0.0001

    colors = [['red', 'green', 'green', 'red' , 'red'],
              ['red', 'red', 'green', 'red', 'red'],
              ['red', 'red', 'green', 'green', 'red'],
              ['red', 'red', 'red', 'red', 'red']]

    (R,C) = matrixSize( colors )

    if 4 == R and 5 == C:
        pass
    else:
        log('++++++++++++ expected 4 by 5 ++++++++++++++++')

    # initialize to uniform dist
    initProb = 1.0 /(R*C)
    initProc = lambda r,c: initProb
    p = buildMatrix(R,C,initProc)

    show(p)

    if closeEnough(1.0, matrixSumUp( p )):
        pass
    else:
        log('++++++++++++ expected sum of p to be 1 ++++++++++++++++')
