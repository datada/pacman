module P where


remove x = filter (/= x)

permutations [] = [[]]
permutations xs = [x:p | x <- xs, p <- permutations (remove x xs)]

main = do
	print $ permutations [1,2,3]
