module Main where

import System.Environment

type State = (Int, [Int], Int)

goalTest :: Int -> Bool
goalTest n = 0 == n `mod` 17

searchD :: [State] -> [State] -> Maybe State
searchD [] _ = Nothing
searchD ((n, moves, cost):toBeVistted) visitedAlready
	| goalTest n = Just (n, moves, cost)
	| otherwise = depthFirst (n, moves, cost) toBeVistted visitedAlready

searchB :: [State] -> [State] -> Maybe State
searchB [] _ = Nothing
searchB ((n, moves, cost):toBeVistted) visitedAlready
	| goalTest n = Just (n, moves, cost)
	| otherwise = breadFirst (n, moves, cost) toBeVistted visitedAlready

depthFirst :: State -> [State] -> [State] -> Maybe State
depthFirst (n, moves, cost) toBeVistted visitedAlready =
	searchD ([(n+1, n:moves, cost), (n+2, n:moves, cost)]++toBeVistted) ((n, moves, cost):visitedAlready)

breadFirst :: State -> [State] -> [State] -> Maybe State
breadFirst (n, moves, cost) toBeVistted visitedAlready =
	searchB (toBeVistted++[(n+2, n:moves, cost), (n+1, n:moves, cost)]) ((n, moves, cost):visitedAlready)

main = do 
	print $ searchD [(1, [], 0)] []
	print $ searchB [(1, [], 0)] []