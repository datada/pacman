module Main where

import System.Environment

goalTest :: Int -> Bool
goalTest n = 0 == n `mod` 17

search :: [Int] -> [Int] -> Maybe [Int]
search [] _ = Nothing
search (x:toBeVistted) visitedAlready
	| goalTest x = Just [x]
	| otherwise = depthFirst x toBeVistted visitedAlready

search2 :: [Int] -> [Int] -> Maybe [Int]
search2 [] _ = Nothing
search2 (x:toBeVistted) visitedAlready
	| goalTest x = Just [x]
	| otherwise = breadFirst x toBeVistted visitedAlready

depthFirst :: Int -> [Int] -> [Int] -> Maybe [Int]
depthFirst here toBeVistted visitedAlready =
	search ([(here+1), (here+2)]++toBeVistted) (here:visitedAlready)

breadFirst :: Int -> [Int] -> [Int] -> Maybe [Int]
breadFirst here toBeVistted visitedAlready =
	search (toBeVistted++[(here+2), (here+1)]) (here:visitedAlready)

main = do 
	print $ search [1, 0, 8] []
	print $ search2 [1, 0, 8] []