module Main where

import System.Environment


type Queen = (Int, Int)

-- diagonally safe means the slope formed by 2 queens is not 1 or -1
isSafe :: Queen -> [Queen] -> Bool
isSafe newQueen [] = True
isSafe newQueen (q:qs)  = validate newQueen q && isSafe newQueen qs
	where validate (x1, y1) (x2, y2) = x1 /= x2 && y1 /= y2 && (abs $ y2 - y1) /= (abs $ x2 - x1)

placeQueens :: Int -> [Int] -> [[Queen]]
placeQueens boardSize [] = [[]]
placeQueens boardSize (c:cs) = [(r, c):otherQueens | r <- [1..boardSize], 
												     otherQueens <- placeQueens boardSize cs,  
												     isSafe (r,c) otherQueens]
-- 8-queens takes forever! without take 1
main = do args <- getArgs
          let n = case args of
          	[] -> 8
          	(a:rgs) -> read a::Int
          print $ take 1 (placeQueens n [1..n])