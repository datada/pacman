module PSet where

powerSet :: [a] -> [[a]]
powerSet [] = [[]]
powerSet (x:xs) = (powerSet xs) ++ map (x:) (powerSet xs)

main = do
	print $ powerSet [1,2,3]
